" Vim syntax file
" Language:	Bullet
" Maintainer:	Lukas Galke <vim@lpag.de>
" Last Change:	2018 Dec 14
" Remark:	Some syntax highlights For bullet journals.

syntax clear


" Modifiers

syntax  match  bulletPriority  /^\s*\zs\*\ze/  contained
syntax  match  bulletInspiration  /^\s*\zs\!\ze/  contained

" Key elements

syntax  match  bulletNote   /^\s*[*!]\?\s*-\s\+/   contains=bulletPriority,bulletInspiration
syntax  match  bulletEvent  /^\s*[*!]\?\s*o\s\+/   contains=bulletPriority,bulletInspiration
syntax  match  bulletTodo   /^\s*[*!]\?\s*\.\s\+/  contains=bulletPriority,bulletInspiration

" Done, migrated or irrelevant Todos

syntax  match  bulletDone        /^\s*[*!]\?\s*\zs[x]\ze\s\+/
syntax  match  bulletMigrated    /^\s*[*!]\?\s*\zs[<>]\ze\s\+/

" These are basically done

hi  link  bulletDone        Comment
hi  link  bulletMigrated    Comment
hi  link  bulletIrrelevant  Comment

hi  link  bulletNote       Statement
hi  link  bulletTodo       Statement
hi  link  bulletEvent      Statement
hi  link  bulletPriority  Todo
hi  link  bulletInspiration  Identifier



