" Vim filetype file
" Language:	Bullet
" Maintainer:	Lukas Galke <vim@lpag.de>
" Last Change:	2018 Dec 14
" Remark:	Filetype for bullet journals

if exists("b:did_ftplugin")
  finish
endif

" Activate formatting lists
setlocal formatoptions+=n
" Set list pattern matching bulletjournal.com
setlocal formatlistpat=^\\s*[*!]\\?\\s*[-.ox<>]\\s\\+

" Small shift width and expand tabs to spaces
setlocal shiftwidth=2 expandtab

" Folding by indent is actually pretty good here
setlocal foldmethod=indent


if exists('b:undo_ftplugin')
  let b:undo_ftplugin .= "|setl fo< flp< sw< et< fdm<"
else
  let b:undo_ftplugin = "setl fo< flp< sw< et< fdm<"
endif

