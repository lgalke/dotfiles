function! s:prepare_copy()
  " Do not wrap lines
  setlocal wrap
  " Don't break inside words
  setlocal linebreak
  " Do not show list chars
  setlocal nolist
  " No spell check
  setlocal nospell
  " No foldcolumn
  setlocal foldcolumn=0
  " No signcolumn
  setlocal signcolumn=no
  " Do not conceal anything
  setlocal conceallevel=0
  " No crosshair
  setlocal nocursorcolumn nocursorline
  setlocal nonumber norelativenumber
  if exists(":ALEDisableBuffer")
    " Disable error highlighting
    ALEDisableBuffer
  endif
  " Open all folds
  normal! zR
endfunction
command! PrepareCopy call<SID>prepare_copy()


" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

function! s:power_safe_mode(on)
  if a:on
    echom "Turning Power Safe mode: ON"
    let g:ale_lint_on_text_changed = "never"
    set lazyredraw
  else
    echom "Turning Power Safe mode: OFF"
    let g:ale_lint_on_text_changed = "always"
    set nolazyredraw
  endif
endfunction
command! -bang SavePower call <SID>power_safe_mode(<bang>1) | doautoall BufReadPost,BufEnter
