" Either '.vim' on unix or 'vimfiles' on windows.
let s:vimdir = fnamemodify(expand('$MYVIMRC'), ':h:t')

call plug#begin('~/' . s:vimdir . '/plugged')

" self {{{
Plug 'lgalke/vim-compiler-vale'
Plug 'lgalke/vim-ernest'
" }}}

" tpope {{{
" The surround map
Plug 'tpope/vim-surround'
" Repeating more operations via dot (.)
Plug 'tpope/vim-repeat'
" Operator-pending commenting via gc
Plug 'tpope/vim-commentary'
" Project-wide configs, alternate files and so on
Plug 'tpope/vim-projectionist'
" Provides `SudoWrite` and `SudoEdit`
Plug 'tpope/vim-eunuch'
" Make and dispatch builds
Plug 'tpope/vim-dispatch'
" Some scripting utilities such as zS to find highlighting group
Plug 'tpope/vim-scriptease'
" End some code blocks properly
Plug 'tpope/vim-endwise'


" Git integration {{{
Plug 'tpope/vim-fugitive'
" Provides `GBrowse` for Github
Plug 'tpope/vim-rhubarb'
" Extension for gitlab
Plug 'shumphrey/fugitive-gitlab.vim'
let g:fugitive_gitlab_domains = ['https://git.kd.informatik.uni-kiel.de', 'https://git.informatik.uni-kiel.de', 'https://gitlab.com']


" }}}
" Guess shiftwidth and other file-specific options
Plug 'tpope/vim-sleuth'
" Short-hand to toggle some options such as `yon` to `set number!`
Plug 'tpope/vim-unimpaired'
" Maps - and provides some improvements over netrw
Plug 'tpope/vim-vinegar'
" Adjust dates with <C-A> and <C-X>
Plug 'tpope/vim-speeddating'

" Auto closing tags via <C-X>/
Plug 'tpope/vim-ragtag'
let g:ragtag_global_maps = 1

" Provides `Subvert` and `Abolish`
Plug 'tpope/vim-abolish'

" Javascript object notation formatting
" Plug 'tpope/vim-jdaddy'

" Database connection, provides :DB
Plug 'tpope/vim-dadbod'
Plug 'vim-airline/vim-airline'

" }}}

" AndrewRadev  {{{
" Argument shifting
Plug 'AndrewRadev/sideways.vim'
nnoremap <a :SidewaysLeft<cr>
nnoremap >a :SidewaysRight<cr>
" suggested by sideways
omap aa <Plug>SidewaysArgumentTextobjA
xmap aa <Plug>SidewaysArgumentTextobjA
omap ia <Plug>SidewaysArgumentTextobjI
xmap ia <Plug>SidewaysArgumentTextobjI

" Delete surrounding function call (maps dsf)
Plug 'AndrewRadev/dsf.vim'

" gS gJ to toggle between one-liners and multi-liners
Plug 'AndrewRadev/splitjoin.vim'

" switch maps gs to switch between True/False etc
Plug 'AndrewRadev/switch.vim'

" Open Preview window :QuickpeekToggle
Plug 'AndrewRadev/quickpeek.vim'

" Edit opening and closing tag simultaneously
Plug 'AndrewRadev/tagalong.vim'

" Diff between lines
Plug 'AndrewRadev/linediff.vim'
" }}}

" File navigation {{{
" Try fzf once again, since junegunns plugins are neat
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
nnoremap <C-P> :FZF<CR>
" One more commands to deal with git
Plug 'junegunn/gv.vim'
" Easy alignment, let's try in favor of tabularize
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'

function! s:goyo_enter()
  set noshowmode
  set noshowcmd
  set scrolloff=999
  ALEDisable
  Limelight
endfunction

function! s:goyo_leave()
  set showmode
  set showcmd
  set scrolloff=5
  ALEEnable
  Limelight!
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()


Plug 'junegunn/vim-easy-align'
vmap <Enter> <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" }}}

" Movement {{{
" Better 's'
Plug 'justinmk/vim-sneak'
let g:sneak#label = 1
" Better 'f'
Plug 'rhysd/clever-f.vim'
" }}}

" Snippets  {{{
if has("python3")
  let g:UltiSnipsUsePythonVersion = 3
elseif has("python")
  let g:UltiSnipsUsePythonVersion = 2
endif
if has("python3") || has("python")
  Plug 'SirVer/UltiSnips'
  Plug 'honza/vim-snippets'
 let g:UltiSnipsExpandTrigger          = "<Tab>"
 let g:UltiSnipsListSnippets           = "<c-k><c-k>"
 let g:UltiSnipsJumpForwardTrigger     = "<c-j>"
 let g:UltiSnipsJumpBackwardTrigger    = "<c-k>"
endif


" The default value for g:UltiSnipsJumpBackwardTrigger interferes with the
" built-in complete function: |i_CTRL-X_CTRL-K|. A workaround is to add the
" following to your vimrc file or switching to a plugin like Supertab or
" YouCompleteMe. 
inoremap <c-x><c-k> <c-x><c-k>

" }}}


" Folding {{{
Plug 'Konfekt/FastFold'
" }}}

" Formatting {{{
let g:latexindent = 1
" }}}

" Python {{{
" Omnicompletion 
" Plug 'davidhalter/jedi-vim'
" " Don't activate automatically, it sucks to wait!
" let g:jedi#popup_on_dot = 0
" let g:jedi#show_call_signatures = 0
" set noshowmode
" Motions and text objects
" Plug 'jeetsukumaran/vim-pythonsense'

" Folding
" Try coiled snake over simpylfold
Plug 'kalekundert/vim-coiled-snake'
" Plug 'tmhedberg/SimpylFold'

" Indent
" Plug 'Vimjas/vim-python-pep8-indent'

Plug 'python-mode/python-mode', { 'branch': 'develop' }
" This is super annoying
" let g:pymode_rope_complete_on_dot = 0


" }}}

" Tagbar {{{ 
Plug 'majutsushi/tagbar'
nnoremap <leader>tt :TagbarToggle<CR>
" }}}

Plug 'freitass/todo.txt-vim'

" Pandoc {{{
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'
let g:pandoc#filetypes#pandoc_markdown = 0
let g:pandoc#biblio#use_bibtool = 1
let g:pandoc#completion#bib#mode = "citeproc"
" }}}

" Linting / ALE {{{
Plug 'dense-analysis/ale' 
let g:ale_echo_msg_format = '[%linter%/%severity%] %...code...%: %s'
let g:ale_linters = { 'python': ['pyls', 'pylint'], 'text': ['writegood']}
let g:ale_fixers = { 'markdown': [ 'remove_trailing_lines', 'trim_whitespace' ], 'python':['isort']}
" Fix with the axe
nnoremap <leader>ax :ALEFix<CR>
" Mnemomic maps
nnoremap <leader>af <Plug>(ale_first)
nnoremap <leader>al <Plug>(ale_last) 
nnoremap <leader>an <Plug>(ale_next_wrap)
nnoremap <leader>ap <Plug>(ale_previous_wrap)
nnoremap <leader>al <Plug>(ale_lint)
nnoremap <leader>ad <Plug>(ale_go_to_definition)
nnoremap <leader>ah <Plug>(ale_ale_hover)

" }}}

" Latex {{{
Plug 'lervag/vimtex'
let g:vimtex_fold_enabled = 1
let g:vimtex_fold_manual = 1
let g:vimtex_format_enabled = 0
let g:vimtex_compiler_latexmk = {
  \ 'backend' : 'jobs',
    \ 'background' : 1,
    \ 'build_dir' : '',
    \ 'callback' : 0,
    \ 'continuous' : 0,
    \ 'executable' : 'latexmk',
    \ 'options' : [
    \   '-pdf',
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \ ],
    \}
" Surround map recommended by vimtex
function! s:latexSurround()
   let b:surround_{char2nr("e")}
     \ = "\\begin{\1environment: \1}\n\t\r\n\\end{\1\1}"
   let b:surround_{char2nr("c")} = "\\\1command: \1{\r}"
endfunction
augroup plugs_ex
  au!
  au FileType tex call s:latexSurround()
augroup END
" }}}

" Colorschemes {{{
Plug 'tpope/vim-vividchalk'
Plug 'sjl/badwolf'
Plug 'morhetz/gruvbox'
Plug 'AlessandroYorba/Alduin'
Plug 'reedes/vim-colors-pencil'
Plug 'jacoborus/tender.vim'

" Low contrast
Plug 'jnurmine/Zenburn'
Plug 'rhysd/vim-color-spring-night'
Plug 'junegunn/seoul256.vim'
Plug 'romainl/Apprentice'
Plug 'arzg/vim-substrata'
Plug 'sainnhe/everforest'




" Minimalist colorschemes
Plug 'robertmeta/nofrils'


" Highlight css colors always
" Plug 'ap/vim-css-color'
" Plug 'chrisbra/Colorizer'
Plug 'gko/vim-coloresque'

"  }}}

" Misc  {{{
Plug 'mhinz/vim-signify'
Plug 'tweekmonster/startuptime.vim'
" }}}

" Organizing {{{
Plug 'vimwiki/vimwiki'
let g:vimwiki_hl_headers = 1
let g:vimwiki_hl_cb_checked = 1
let g:vimwiki_folding = 'expr'
augroup vimwiki_autocommands
  au!
  au BufRead *diary.wiki VimwikiDiaryGenerateLinks
augroup END
" }}}

call plug#end()
