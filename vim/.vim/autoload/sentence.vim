function! sentence#format_deprecated() abort
  " At most one sentence per line (but could be less)
  " Sentence regexp derived from `:help sentence`

  " If formatexpr is called due to autoformat or textwidth, do nothing.
  if mode() =~# '[iR]' | return -1 | endif
  " Fall back to the internal format mechanism, see ':help formatexpr'

  " TODO FIXME should not format comments...
  " let l:cms_pat = '\(' . substitute(&commentstring, '%s.*', '.\{-}', '') . '\)'
  " Get first comment marker
  " let l:comment_marker = split(&commentstring, '%s')[0]

  let l:save_cursor = getcurpos()
  " See :help sentence
  execute v:lnum . 'smagic/[.!?][])"'']\{-}\zs\s\+\ze\w/\r/eg'.v:count
  " execute v:lnum . 's/\m\(^\s*\V'.l:comment_marker.'\m\s*\)\?.\{-}\m[.!?][])"'']\{-}\zs\s\+\ze\w/\r\1/eg'.v:count
  call setpos('.', save_cursor)

endfunction


function! sentence#format() abort range
  " Do nothing if called due to textwidth/autoformat
  if mode() =~# '[iR]' | return -1 | endif
  " let l:save_cursor = getcurpos()
  " Regexp for sentence boundaries (see :help sentence)
  let l:sentence_boundary = '\m[.!?][])"'']\{-}\zs\s\+\ze'

  " Retrieve comment (opening) marker for current filetype
  if &commentstring != ''
    let l:comment_marker = split(&commentstring, '%s')[0]
  endif

  " Sentences to fill with result
  let l:sentences = []
  " v:count holds absolute line count, including start
  " for loop in vimscript include the upper bound
  echo v:lnum . ' to ' . (v:lnum + v:count - 1)
  for l:line in getline(v:lnum, v:lnum + v:count - 1)
    if l:line == ''
      " Special treatment for empty line
      l:sentences += ['']
    endif
    " TODO FIXME comment marker can be empty
    if l:comment_marker && l:line =~# '\m^\s*\V'.l:comment_marker
      " Full comment line, easy. Do nothing with it.
      let l:sentences += [l:line]
      " It needs special treatment because non-escaped split is difficult if
      " comment marker is the first char of a line
    else
      " Process only sentences before comment marker.
      " Be careful with escaped comment chars. Guess escape char is '\'.
      let [l:text; l:rest] = split(l:line, '\m[^\\]\V\zs'.l:comment_marker.'\ze')
      " Split by sentence delimiter and add to result
      let l:sentences += split(l:text, l:sentence_boundary)
      " Add rest (might be empty), reverting split on comment marker
      if len(l:rest)
        let l:sentences += [l:comment_marker . join(l:rest, l:comment_marker)]
      endif
    endif
  endfor
  " Delete old line(s)
  silent execute v:lnum . ',' . (v:lnum+v:count-1) . 'delete'
  " Append new lien (-1 because append starts *below* that line)
  echom l:sentences
  call append(v:lnum - 1, l:sentences)
  " Move to begin of new lines
  " silent execute 'normal! ' . v:lnum . 'G'
  " Re-indent amount of new lines
  " silent execute 'normal! ' . len(l:sentences) . '=='
  " Restore old cursor position
  " TODO The cursor is left on the first non-blank of the last formatted line.
  let l:endcol = v:lnum+len(l:sentences)-1
  call setpos('.', [0, l:endcol, 1, 0])
endfunction
